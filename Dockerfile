FROM gradle:jdk11-focal AS build
WORKDIR /buildah
COPY . /buildah
RUN gradle build --no-daemon 

FROM adoptopenjdk/openjdk11:x86_64-alpine-jre-11.0.19_7
WORKDIR /app
COPY --from=build /buildah/build/libs/*.jar /app/app.jar
CMD [ "java", "-jar", "app.jar" ]